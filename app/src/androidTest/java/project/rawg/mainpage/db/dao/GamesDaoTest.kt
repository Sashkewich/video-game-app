package project.rawg.mainpage.db.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import project.rawg.mainpage.api.GenreType
import project.rawg.mainpage.db.database.GamesDataBase
import project.rawg.mainpage.db.model.GameEntity
import project.rawg.mainpage.db.model.RatingEntity
import project.rawg.mainpage.db.model.ShortScreenshotEntity
import project.rawg.utils.extensions.collectFLow

@RunWith(AndroidJUnit4::class)
@SmallTest
class GamesDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    private lateinit var database: GamesDataBase
    private lateinit var dao: GamesDao
    private lateinit var testDispatcher: TestDispatcher

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setup() {
        testDispatcher = StandardTestDispatcher()
        Dispatchers.setMain(testDispatcher)
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            GamesDataBase::class.java
        ).allowMainThreadQueries().build()

        dao = database.gamesDao
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @After
    fun teardown() {
        database.close()
        Dispatchers.resetMain()
        testDispatcher.cancel()
    }

    @Test
    fun addGames() {
        runTest(testDispatcher) {
            val games = listOf(
                GameEntity(
                    id = 1,
                    gameId = 1,
                    slug = "game",
                    name = "Game",
                    genres = "Action",
                    genreType = GenreType.Action.genreTitle,
                    released = "22",
                    backgroundImage = "1",
                    rating = 5f,
                    ratingsCount = 3,
                    metacritic = null,
                    platforms = "pc",
                    esrbRatingName = null,
                    esrbRatingIcon = null
                )
            )
            dao.addGames(games)

            val existingGames =
                dao.getGames(GenreType.Action.genreTitle).collectFLow(testDispatcher, this) { it }

            assertEquals(existingGames, games)
            this.coroutineContext.cancelChildren()
        }
    }

    @Test
    fun getGames() {
        runTest(testDispatcher) {
            val existingGames =
                dao.getGames(GenreType.Action.genreTitle).collectFLow(testDispatcher, this) { it }

            assertEquals(existingGames, emptyList<GameEntity>())
            this.coroutineContext.cancelChildren()
        }
    }

    @Test
    fun addRatings() {
        runTest(testDispatcher) {
            val ratingMinecraft = RatingEntity(
                ratingId = 1,
                gameId = 1,
                id = 5,
                title = "exceptional",
                count = 99999,
                percent = 99f
            )
            val ratingWarface = RatingEntity(
                ratingId = 5,
                gameId = 666,
                id = 1,
                title = "skip",
                count = 99999,
                percent = 99f
            )
            val ratings = listOf(
                ratingMinecraft,
                ratingWarface
            )
            dao.addRatings(ratings)

            val existingRatings = dao.getRatings(1).collectFLow(testDispatcher, this) { it }
            assertEquals(existingRatings[0], ratingMinecraft)
            this.coroutineContext.cancelChildren()
        }
    }

    @Test
    fun getRatings() {
        runTest(testDispatcher) {
            val existingRatings = dao.getRatings(1).collectFLow(testDispatcher, this) { it }
            assertEquals(existingRatings, emptyList<RatingEntity>())
            this.coroutineContext.cancelChildren()
        }
    }

    @Test
    fun addShortScreenshots() {
        runTest(testDispatcher) {
            val screenshots = listOf(
                ShortScreenshotEntity(1, "911"),
                ShortScreenshotEntity(1, image = "199")
            )
            dao.addShortScreenshots(screenshots)

            val existingScreenshots =
                dao.getShortScreenshots(1).collectFLow(testDispatcher, this) { it }

            assertEquals(screenshots, existingScreenshots)
            this.coroutineContext.cancelChildren()
        }
    }

    @Test
    fun getShortScreenshots() {
        runTest(testDispatcher) {
            val existingScreenshots =
                dao.getShortScreenshots(2).collectFLow(testDispatcher, this) { it }

            assertEquals(existingScreenshots, emptyList<ShortScreenshotEntity>())
            this.coroutineContext.cancelChildren()
        }
    }
}