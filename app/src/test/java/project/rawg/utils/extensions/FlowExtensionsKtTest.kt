package project.rawg.utils.extensions

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import project.rawg.mainpage.db.model.RatingEntity
import project.rawg.mainpage.model.Rating

class FlowExtensionsKtTest {

    private lateinit var data: MutableList<Rating>
    private lateinit var mappedData: MutableList<RatingEntity>
    private lateinit var testDispatcher: CoroutineDispatcher
    private lateinit var range: IntRange
    private lateinit var flow: Flow<List<Rating>>

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setup() {
        testDispatcher = UnconfinedTestDispatcher()
        data = mutableListOf()
        mappedData = mutableListOf()
        range = IntRange(1, 5)
        range.forEach {
            data.add(
                Rating(
                    id = it,
                    title = it.toString(),
                    count = it,
                    percent = it.toFloat()
                )
            )
            mappedData.add(
                RatingEntity(
                    gameId = it,
                    id = it,
                    title = it.toString(),
                    count = it,
                    percent = it.toFloat()
                )
            )
        }
        flow = flow { emit(data) }
    }

    @Test
    fun `collect correct type of data from flow, return true`() {
        runTest {
            val actualData = flow.collectFLow(testDispatcher, this) {
                it
            }
            val condition = (data == actualData)
            assertTrue(condition)
        }
    }

    @Test
    fun `collect incorrect type of data from flow, return false`() {
        runTest {
            val actualData = flow.collectFLow(testDispatcher, this) { list ->
                list.map {
                    it.toRatingEntity(it.id)
                }
            }
            val condition = (data != actualData)
            assertTrue(condition)
        }
    }

    @Test
    fun `collect correct type of data from flow with mapping, return true`() {
        runTest {
            val actualData = flow.collectFLow(testDispatcher, this) { list ->
                list.map {
                    it.toRatingEntity(it.id)
                }
            }
            val condition = (mappedData == actualData)
            assertTrue(condition)
        }
    }

    @Test
    fun `collect incorrect type of data from flow with mapping, return false`() {
        runTest {
            val actualData = flow.collectFLow(testDispatcher, this) {
                it
            }
            val condition = (mappedData != actualData)
            assertTrue(condition)
        }
    }
}