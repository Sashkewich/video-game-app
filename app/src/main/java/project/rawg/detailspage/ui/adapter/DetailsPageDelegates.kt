package project.rawg.detailspage.ui.adapter

import coil.load
import coil.transform.RoundedCornersTransformation
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import project.rawg.databinding.ItemScreenshotBinding

object DetailsPageDelegates {

    fun screenshotDelegate() = adapterDelegateViewBinding<String, String, ItemScreenshotBinding>(
        { inflater, container -> ItemScreenshotBinding.inflate(inflater, container, false) }
    ) {
        bind {
            binding.imageViewScreenshot.load(item) {
                crossfade(true)
                crossfade(600)
                transformations(RoundedCornersTransformation(16f))
            }
        }
    }
}
