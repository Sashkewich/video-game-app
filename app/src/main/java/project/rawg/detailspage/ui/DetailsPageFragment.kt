package project.rawg.detailspage.ui

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import coil.load
import project.rawg.R
import project.rawg.common.mvvm.BaseFragment
import project.rawg.databinding.FragmentDetailsPageBinding
import project.rawg.detailspage.ui.adapter.DetailsPageAdapter
import project.rawg.mainpage.ui.model.game.GameUi
import project.rawg.utils.Arguments
import project.rawg.utils.extensions.args
import project.rawg.utils.extensions.popScreen
import project.rawg.utils.extensions.withArgs
import project.rawg.utils.viewbinding.viewBinding


class DetailsPageFragment : BaseFragment(R.layout.fragment_details_page) {

    private val binding: FragmentDetailsPageBinding by viewBinding()

    companion object {
        fun newInstance(game: GameUi) =
            DetailsPageFragment().withArgs(Arguments.GAME_DATA to game)
    }

    private val game: GameUi by args(Arguments.GAME_DATA)

    private val adapter by lazy {
        DetailsPageAdapter()
    }

    override fun bind() {
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.toolbar)
        with(binding) {
            textViewName.text = game.name
            textViewScore.text = game.rating.toString()
            ratingBar.rating = game.rating
            textViewReleased.append(game.released)
            textViewGenres.append(game.genres)
            textViewPlatforms.append(game.parentPlatforms)
            textViewAgeRating.text = game.esrbRatingName
            adapter.items = game.shortScreenshots

            toolbar.setNavigationOnClickListener {
                popScreen()
            }
        }
    }

    override fun initViews(view: View) {
        with(binding) {
            recyclerViewScreenshots.adapter = adapter
            recyclerViewScreenshots.setHasFixedSize(true)
            adapter.onAttachedToRecyclerView(recyclerViewScreenshots)
        }
    }
}