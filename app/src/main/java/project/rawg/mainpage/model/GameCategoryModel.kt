package project.rawg.mainpage.model

import project.rawg.mainpage.api.PagingState
import project.rawg.mainpage.api.GenreType

data class GameCategoryModel(
    val genreType: GenreType,
    val dataState: PagingState<List<Game>>
)
