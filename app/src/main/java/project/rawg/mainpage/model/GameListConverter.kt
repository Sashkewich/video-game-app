package project.rawg.mainpage.model

import project.rawg.mainpage.api.model.*

interface GameConverter {
    suspend fun convert(responseData: List<GameResponse>): List<Game>
}

interface RatingConverter {
    suspend fun convert(responseData: List<RatingResponse>): List<Rating>
}

interface ParentPlatformConverter {
    suspend fun convert(responseData: List<ParentPlatformResponse>): String
}

interface GenreConverter {
    suspend fun convert(responseData: List<GenreResponse>): String
}

interface ShortScreenshotConverter {
    suspend fun convert(responseData: List<ShortScreenshotResponse>): List<String>
}
