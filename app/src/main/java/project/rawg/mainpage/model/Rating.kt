package project.rawg.mainpage.model

import project.rawg.mainpage.db.model.RatingEntity

data class Rating(
    val id: Int,
    val title: String,
    val count: Int,
    val percent: Float
) {
    fun toRatingEntity(gameId: Int) = RatingEntity(
        gameId = gameId,
        id = id,
        title = title,
        count = count,
        percent = percent
    )
}
