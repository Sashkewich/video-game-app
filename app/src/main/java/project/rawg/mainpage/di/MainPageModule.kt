package project.rawg.mainpage.di

import android.content.Context
import androidx.room.Room
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import project.rawg.mainpage.api.MainPageApi
import project.rawg.mainpage.db.dao.GamesDao
import project.rawg.mainpage.db.database.GamesDataBase
import project.rawg.mainpage.db.database.GamesMediator
import project.rawg.mainpage.api.GenreType
import project.rawg.mainpage.interactor.MainPageInteractorImpl
import project.rawg.mainpage.interactor.base.MainPageInteractor
import project.rawg.mainpage.repository.MainPageLocalRepositoryImpl
import project.rawg.mainpage.repository.MainPageRemoteRepositoryImpl
import project.rawg.mainpage.repository.base.MainPageLocalRepository
import project.rawg.mainpage.repository.base.MainPageRemoteRepository
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Qualifier
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
abstract class MainPageModule {

    @Binds
    @Singleton
    abstract fun bindRemoteRepository(repository: MainPageRemoteRepositoryImpl): MainPageRemoteRepository

    @Binds
    @Singleton
    abstract fun bindLocalRepository(repository: MainPageLocalRepositoryImpl): MainPageLocalRepository

    @Binds
    @Singleton
    abstract fun bindInteractor(interactor: MainPageInteractorImpl): MainPageInteractor

    companion object {

        @IoDispatcher
        @Provides
        fun provideContextIo(): CoroutineDispatcher = Dispatchers.IO

        @DefaultDispatcher
        @Provides
        fun provideContextDefault(): CoroutineDispatcher = Dispatchers.Default

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class IoDispatcher

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class DefaultDispatcher


        @Provides
        @Singleton
        fun provideApi(retrofit: Retrofit): MainPageApi =
            retrofit.create(MainPageApi::class.java)

        @Provides
        @Singleton
        fun provideChannelDao(appDatabase: GamesDataBase): GamesDao =
            appDatabase.gamesDao

        @Provides
        @Singleton
        fun provideAppDatabase(@ApplicationContext appContext: Context): GamesDataBase =
            Room.databaseBuilder(
                appContext,
                GamesDataBase::class.java,
                "Database"
            ).build()

        @Provides
        @Singleton
        @Named("action")
        fun actionMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Action,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("indie")
        fun indieMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Indie,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("adventure")
        fun adventureMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Adventure,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("rpg")
        fun rpgMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Rpg,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("strategy")
        fun strategyMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Strategy,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("shooter")
        fun shooterMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Shooter,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("casual")
        fun casualMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Casual,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("simulation")
        fun simulationMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Simulation,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("puzzle")
        fun puzzleMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Puzzle,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("arcade")
        fun arcadeMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Arcade,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("platformer")
        fun platformerMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Platformer,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("massive")
        fun massiveMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.MassivelyMultiplayer,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("racing")
        fun racingMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Racing,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("sports")
        fun sportsMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Sports,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("fighting")
        fun fightingMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Fighting,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("family")
        fun familyMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Family,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("board")
        fun boardMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.BoardGames,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("edu")
        fun eduMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Educational,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("card")
        fun cardMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreType.Card,
            defaultDispatcher
        )
    }
}
