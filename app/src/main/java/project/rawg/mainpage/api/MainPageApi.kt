package project.rawg.mainpage.api

import project.rawg.mainpage.api.model.GameListResponse
import project.rawg.utils.Constants
import project.rawg.utils.Constants.DEFAULT_PAGE
import project.rawg.utils.Constants.PAGE_SIZE
import retrofit2.http.GET
import retrofit2.http.Query

interface MainPageApi {
    @GET("api/games")
    suspend fun getGameList(
        @Query("key") key: String = Constants.API_KEY,
        @Query("genres") genre: String,
        @Query("page_size") pageSize: Int = PAGE_SIZE,
        @Query("page") page: Int = DEFAULT_PAGE
    ): GameListResponse
}
