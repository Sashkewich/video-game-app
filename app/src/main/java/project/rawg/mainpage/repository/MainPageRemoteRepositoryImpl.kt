package project.rawg.mainpage.repository

import androidx.annotation.DrawableRes
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import project.rawg.R
import project.rawg.mainpage.api.MainPageApi
import project.rawg.mainpage.api.model.*
import project.rawg.mainpage.di.MainPageModule.Companion.DefaultDispatcher
import project.rawg.mainpage.di.MainPageModule.Companion.IoDispatcher
import project.rawg.mainpage.model.*
import project.rawg.mainpage.repository.base.MainPageRemoteRepository
import project.rawg.utils.Constants
import project.rawg.utils.Constants.DEFAULT_PAGE
import javax.inject.Inject

class MainPageRemoteRepositoryImpl @Inject constructor(
    private val api: MainPageApi,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) : MainPageRemoteRepository {

    private var genre: String? = ""
    private var page = DEFAULT_PAGE

    override fun updateParams(genre: String, alreadyLoadedCount: Int) {
        this.genre = genre
        if (alreadyLoadedCount < 0) throw IllegalArgumentException()
        page = (alreadyLoadedCount / (Constants.PAGE_SIZE + 1)) + 1
    }

    override suspend fun initialLoading(genre: String): List<Game> {
        return withContext(ioDispatcher) {
            val response = api.getGameList(genre = genre)
            this@MainPageRemoteRepositoryImpl.genre = genre
            gameConverter.convert(response.results)
        }
    }

    override suspend fun loadMore(): List<Game> {
        return withContext(ioDispatcher) {
            val genre = this@MainPageRemoteRepositoryImpl.genre ?: throw IllegalStateException()
            val response = api.getGameList(genre = genre, page = page + 1)
            page += 1
            gameConverter.convert(response.results)
        }
    }

    private val ratingConverter: RatingConverter = object : RatingConverter {
        override suspend fun convert(responseData: List<RatingResponse>): List<Rating> {
            return withContext(defaultDispatcher) {
                responseData.map {
                    Rating(
                        id = it.id,
                        title = it.title,
                        count = it.count,
                        percent = it.percent
                    )
                }
            }
        }
    }

    private val parentPlatformConverter: ParentPlatformConverter =
        object : ParentPlatformConverter {
            override suspend fun convert(responseData: List<ParentPlatformResponse>): String {
                return withContext(defaultDispatcher) {
                    val platforms = buildString {
                        responseData.forEach {
                            append(it.parentPlatformInfo.name).append(", ")
                        }
                    }
                    platforms.dropLast(2)
                }
            }
        }

    private val genreConverter: GenreConverter = object : GenreConverter {
        override suspend fun convert(responseData: List<GenreResponse>): String {
            return withContext(defaultDispatcher) {
                val genres = buildString {
                    responseData.forEach {
                        append(it.name).append(", ")
                    }
                }
                genres.dropLast(2)
            }
        }
    }

    private val shortScreenshotConverter: ShortScreenshotConverter =
        object : ShortScreenshotConverter {
            override suspend fun convert(responseData: List<ShortScreenshotResponse>): List<String> {
                return withContext(defaultDispatcher) {
                    responseData.map {
                        it.image
                    }
                }
            }
        }


    private val gameConverter: GameConverter = object : GameConverter {
        override suspend fun convert(responseData: List<GameResponse>): List<Game> {
            return withContext(defaultDispatcher) {
                responseData.map {
                    Game(
                        id = it.id,
                        slug = it.slug,
                        name = it.name,
                        released = it.released,
                        backgroundImage = it.backgroundImage,
                        rating = it.rating,
                        ratings = ratingConverter.convert(it.ratings),
                        ratingsCount = it.ratingsCount,
                        metacritic = it.metacritic ?: 0,
                        parentPlatforms = parentPlatformConverter.convert(it.parentPlatforms),
                        genres = genreConverter.convert(it.genres),
                        esrbRatingName = it.esrbRating?.name ?: "",
                        esrbRatingIcon = getIcon(it.esrbRating?.name),
                        shortScreenshots = shortScreenshotConverter.convert(it.shortScreenshots)
                    )
                }
            }
        }
    }

    @DrawableRes
    private fun getIcon(ratingName: String?): Int? =
        when (ratingName) {
            RatingType.RATING_PENDING.type -> R.drawable.esrb_rp
            RatingType.EVERYONE.type -> R.drawable.esrb_everyone
            RatingType.EVERYONE_TEN.type -> R.drawable.esrb_everyone_10_
            RatingType.TEEN.type -> R.drawable.esrb_teen
            RatingType.MATURE.type -> R.drawable.esrb_mature_17_
            RatingType.ADULTS_ONLY.type -> R.drawable.esrb_adults_only_18_
            else -> null
        }
}
