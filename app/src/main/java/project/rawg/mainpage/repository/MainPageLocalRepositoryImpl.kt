package project.rawg.mainpage.repository

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import project.rawg.mainpage.db.dao.GamesDao
import project.rawg.mainpage.db.model.RatingEntity
import project.rawg.mainpage.db.model.ShortScreenshotEntity
import project.rawg.mainpage.di.MainPageModule.Companion.DefaultDispatcher
import project.rawg.mainpage.model.Game
import project.rawg.mainpage.repository.base.MainPageLocalRepository
import project.rawg.utils.extensions.collectFLow
import javax.inject.Inject

class MainPageLocalRepositoryImpl @Inject constructor(
    private val dao: GamesDao, @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) : MainPageLocalRepository {


    override suspend fun getGamesList(scope: CoroutineScope, genre: String): Flow<List<Game>> {
        val games = dao.getGames(genre)

        return games.flowOn(defaultDispatcher).map { list ->
            list.map { game ->
                game.toGame(dao.getRatings(game.id).collectFLow(
                    defaultDispatcher,
                    scope,
                ) { ratings ->
                    ratings.map {
                        it.toRating()
                    }.distinctBy { it.id }
                }, shortScreenshots = dao.getShortScreenshots(game.id).collectFLow(
                    defaultDispatcher,
                    scope,
                ) { shortScreenshots ->
                    shortScreenshots.map {
                        it.image
                    }
                })
            }
        }
    }

    override suspend fun putGamesList(data: List<Game>, genre: String) {
        withContext(defaultDispatcher) {
            val gamesEntity = data.map {
                it.toGameEntity(genre)
            }
            dao.addGames(gamesEntity)


            val ratingEntity
                    : MutableList<RatingEntity> = mutableListOf()
            data.forEach { game ->
                ratingEntity += game.ratings.map {
                    it.toRatingEntity(game.id)
                }
            }
            dao.addRatings(ratingEntity)

            val shortScreenshotEntity
                    : MutableList<ShortScreenshotEntity> = mutableListOf()
            data.forEach { game ->
                shortScreenshotEntity += game.shortScreenshots.map {
                    ShortScreenshotEntity(game.id, it)
                }
            }
            dao.addShortScreenshots(shortScreenshotEntity)
        }
    }
}
