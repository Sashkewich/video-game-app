package project.rawg.mainpage.ui

import android.os.Parcelable
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import project.rawg.R
import project.rawg.common.mvvm.BaseFragment
import project.rawg.databinding.FragmentMainPageBinding
import project.rawg.detailspage.ui.DetailsPageFragment
import project.rawg.mainpage.ui.adapter.MainPageGameAdapter
import project.rawg.mainpage.ui.model.base.BaseGame
import project.rawg.mainpage.ui.model.game.GameUi
import project.rawg.utils.extensions.addScreen
import project.rawg.utils.viewbinding.viewBinding

@AndroidEntryPoint
class MainPageFragment : BaseFragment(R.layout.fragment_main_page) {
    private val binding: FragmentMainPageBinding by viewBinding()
    private val viewModel: MainPageViewModel by viewModels()
    private val scrollStates: MutableMap<Int, Parcelable?> = mutableMapOf()

    private val adapter by lazy {
        MainPageGameAdapter(
            onItemBind = viewModel::initCategory,
            onReadyToLoadMore = viewModel::readyToLoadMore,
            onGameClick = ::onGameClick,
            onRefreshClick = viewModel::refresh,
            scrollStates = scrollStates
        )
    }

    override fun bind() {
        with(viewModel) {
            observe(gameListStateFlow) { adapter.items = it }
        }
    }

    override fun initViews(view: View) {
        with(binding) {
            recyclerViewGenres.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            recyclerViewGenres.adapter = adapter
            recyclerViewGenres.setHasFixedSize(true)
            recyclerViewGenres.itemAnimator = null
            adapter.onAttachedToRecyclerView(recyclerViewGenres)
        }
    }

    private fun onGameClick(game: BaseGame) {
        addScreen(DetailsPageFragment.newInstance(game as GameUi))
    }
}
