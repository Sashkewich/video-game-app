package project.rawg.mainpage.interactor

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import project.rawg.mainpage.api.GenreType
import project.rawg.mainpage.db.database.GamesMediator
import project.rawg.mainpage.interactor.base.MainPageInteractor
import project.rawg.mainpage.model.*
import project.rawg.mainpage.ui.model.*
import project.rawg.mainpage.ui.model.base.BaseGameList
import project.rawg.mainpage.ui.model.game.*
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

class MainPageInteractorImpl @Inject constructor(
    @Named("action") private val actionMediator: GamesMediator,
    @Named("indie") private val indieMediator: GamesMediator,
    @Named("adventure") private val adventureMediator: GamesMediator,
    @Named("rpg") private val rpgMediator: GamesMediator,
    @Named("strategy") private val strategyMediator: GamesMediator,
    @Named("shooter") private val shooterMediator: GamesMediator,
    @Named("casual") private val casualMediator: GamesMediator,
    @Named("simulation") private val simulationMediator: GamesMediator,
    @Named("puzzle") private val puzzleMediator: GamesMediator,
    @Named("arcade") private val arcadeMediator: GamesMediator,
    @Named("platformer") private val platformerMediator: GamesMediator,
    @Named("massive") private val massivelyMultiplayerMediator: GamesMediator,
    @Named("racing") private val racingMediator: GamesMediator,
    @Named("sports") private val sportsMediator: GamesMediator,
    @Named("fighting") private val fightingMediator: GamesMediator,
    @Named("family") private val familyMediator: GamesMediator,
    @Named("board") private val boardGamesMediator: GamesMediator,
    @Named("edu") private val educationalMediator: GamesMediator,
    @Named("card") private val cardMediator: GamesMediator
) : MainPageInteractor {
    override fun data(): Flow<List<BaseGameList>> = combine(
        actionMediator.data(),
        indieMediator.data(),
        adventureMediator.data(),
        rpgMediator.data(),
        strategyMediator.data(),
        shooterMediator.data(),
        casualMediator.data(),
        simulationMediator.data(),
        puzzleMediator.data(),
        arcadeMediator.data(),
        platformerMediator.data(),
        massivelyMultiplayerMediator.data(),
        racingMediator.data(),
        sportsMediator.data(),
        fightingMediator.data(),
        familyMediator.data(),
        boardGamesMediator.data(),
        educationalMediator.data(),
        cardMediator.data()
    ) { values ->
        values.map { gameCategory ->
            GameListConverter.toGameListUi(gameCategory)
        }
    }

    override suspend fun initCategory(genre: GenreType, scope: CoroutineScope) {
        when (genre) {
            is GenreType.Action -> actionMediator.init(scope)
            is GenreType.Indie -> indieMediator.init(scope)
            is GenreType.Adventure -> adventureMediator.init(scope)
            is GenreType.Rpg -> rpgMediator.init(scope)
            is GenreType.Strategy -> strategyMediator.init(scope)
            is GenreType.Shooter -> shooterMediator.init(scope)
            is GenreType.Casual -> casualMediator.init(scope)
            is GenreType.Simulation -> simulationMediator.init(scope)
            is GenreType.Puzzle -> puzzleMediator.init(scope)
            is GenreType.Arcade -> arcadeMediator.init(scope)
            is GenreType.Platformer -> platformerMediator.init(scope)
            is GenreType.MassivelyMultiplayer -> massivelyMultiplayerMediator.init(scope)
            is GenreType.Racing -> racingMediator.init(scope)
            is GenreType.Sports -> sportsMediator.init(scope)
            is GenreType.Fighting -> fightingMediator.init(scope)
            is GenreType.Family -> familyMediator.init(scope)
            is GenreType.BoardGames -> boardGamesMediator.init(scope)
            is GenreType.Educational -> educationalMediator.init(scope)
            is GenreType.Card -> cardMediator.init(scope)
        }
    }

    override suspend fun tryToLoadMore(genre: GenreType, index: Int, scope: CoroutineScope) {
        when (genre) {
            is GenreType.Action -> actionMediator.tryToLoadMore(index, scope)
            is GenreType.Indie -> indieMediator.tryToLoadMore(index, scope)
            is GenreType.Adventure -> adventureMediator.tryToLoadMore(index, scope)
            is GenreType.Rpg -> rpgMediator.tryToLoadMore(index, scope)
            is GenreType.Strategy -> strategyMediator.tryToLoadMore(index, scope)
            is GenreType.Shooter -> shooterMediator.tryToLoadMore(index, scope)
            is GenreType.Casual -> casualMediator.tryToLoadMore(index, scope)
            is GenreType.Simulation -> simulationMediator.tryToLoadMore(index, scope)
            is GenreType.Puzzle -> puzzleMediator.tryToLoadMore(index, scope)
            is GenreType.Arcade -> arcadeMediator.tryToLoadMore(index, scope)
            is GenreType.Platformer -> platformerMediator.tryToLoadMore(index, scope)
            is GenreType.MassivelyMultiplayer -> massivelyMultiplayerMediator.tryToLoadMore(index, scope)
            is GenreType.Racing -> racingMediator.tryToLoadMore(index, scope)
            is GenreType.Sports -> sportsMediator.tryToLoadMore(index, scope)
            is GenreType.Fighting -> fightingMediator.tryToLoadMore(index, scope)
            is GenreType.Family -> familyMediator.tryToLoadMore(index, scope)
            is GenreType.BoardGames -> boardGamesMediator.tryToLoadMore(index, scope)
            is GenreType.Educational -> educationalMediator.tryToLoadMore(index, scope)
            is GenreType.Card -> cardMediator.tryToLoadMore(index, scope)
        }
    }

    override suspend fun refresh(genre: GenreType, scope: CoroutineScope) {
        when (genre) {
            is GenreType.Action -> actionMediator.refresh(scope)
            is GenreType.Indie -> indieMediator.refresh(scope)
            is GenreType.Adventure -> adventureMediator.refresh(scope)
            is GenreType.Rpg -> rpgMediator.refresh(scope)
            is GenreType.Strategy -> strategyMediator.refresh(scope)
            is GenreType.Shooter -> shooterMediator.refresh(scope)
            is GenreType.Casual -> casualMediator.refresh(scope)
            is GenreType.Simulation -> simulationMediator.refresh(scope)
            is GenreType.Puzzle -> puzzleMediator.refresh(scope)
            is GenreType.Arcade -> arcadeMediator.refresh(scope)
            is GenreType.Platformer -> platformerMediator.refresh(scope)
            is GenreType.MassivelyMultiplayer -> massivelyMultiplayerMediator.refresh(scope)
            is GenreType.Racing -> racingMediator.refresh(scope)
            is GenreType.Sports -> sportsMediator.refresh(scope)
            is GenreType.Fighting -> fightingMediator.refresh(scope)
            is GenreType.Family -> familyMediator.refresh(scope)
            is GenreType.BoardGames -> boardGamesMediator.refresh(scope)
            is GenreType.Educational -> educationalMediator.refresh(scope)
            is GenreType.Card -> cardMediator.refresh(scope)
        }
    }
}
