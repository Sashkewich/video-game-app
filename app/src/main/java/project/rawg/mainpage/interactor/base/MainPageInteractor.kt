package project.rawg.mainpage.interactor.base

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import project.rawg.mainpage.api.GenreType
import project.rawg.mainpage.ui.model.base.BaseGameList

interface MainPageInteractor {
    fun data(): Flow<List<BaseGameList>>
    suspend fun initCategory(genre: GenreType, scope: CoroutineScope)
    suspend fun tryToLoadMore(genre: GenreType, index: Int, scope: CoroutineScope)
    suspend fun refresh(genre: GenreType, scope: CoroutineScope)
}