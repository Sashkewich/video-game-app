package project.rawg.mainpage.interactor

import project.rawg.mainpage.api.PagingState
import project.rawg.mainpage.model.Game
import project.rawg.mainpage.model.GameCategoryModel
import project.rawg.mainpage.ui.model.game.*

object GameListConverter {

    private val INITIAL_PROGRESS_RANGE = IntRange(1, 3)

    fun toGameListUi(model: GameCategoryModel): GameListUi =
        when (model.dataState) {
            is PagingState.Initial ->
                GameListUi(
                    genre = model.genreType,
                    results = INITIAL_PROGRESS_RANGE
                        .map { ProgressGame }
                )

            is PagingState.Content ->
                GameListUi(
                    genre = model.genreType,
                    results = model.dataState.data.toGameUiList()
                )

            is PagingState.Paging ->
                GameListUi(
                    genre = model.genreType,
                    results = model.dataState.availableContent.toGameUiList()
                        .plus(ProgressGame)
                )

            is PagingState.Persist ->
                GameListUi(
                    genre = model.genreType,
                    results = model.dataState.data.toGameUiList()
                        .plus(ErrorItem(model.genreType))
                )

            is PagingState.Error ->
                GameListUi(
                    genre = model.genreType,
                    results = listOf(ErrorItem(model.genreType))
                )

        }

    private fun List<Game>.toGameUiList() = map {
        GameUi(
            id = it.id,
            slug = it.slug,
            name = it.name,
            released = it.released,
            backgroundImage = it.backgroundImage,
            rating = it.rating,
            ratings = it.ratings.map { rating ->
                RatingUi(
                    id = rating.id,
                    title = rating.title,
                    count = rating.count,
                    percent = rating.percent
                )
            },
            ratingsCount = it.ratingsCount,
            metacritic = it.metacritic.toString(),
            parentPlatforms = it.parentPlatforms,
            genres = it.genres,
            esrbRatingName = it.esrbRatingName,
            esrbRatingIcon = it.esrbRatingIcon,
            shortScreenshots = it.shortScreenshots
        )
    }
}