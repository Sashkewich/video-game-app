package project.rawg.mainpage.db.database

import androidx.room.Database
import androidx.room.RoomDatabase
import project.rawg.mainpage.db.dao.GamesDao
import project.rawg.mainpage.db.model.*

@Database(
    entities = [
        RatingEntity::class, ShortScreenshotEntity::class,
        GameEntity::class
    ],
    version = 1,
    exportSchema = false
)

abstract class GamesDataBase : RoomDatabase() {
    abstract val gamesDao: GamesDao
}
