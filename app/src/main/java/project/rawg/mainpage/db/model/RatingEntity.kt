package project.rawg.mainpage.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import project.rawg.mainpage.model.Rating

@Entity(
    tableName = "ratings",
    indices = [Index(value = ["id", "game_id"], unique = true)]
)
data class RatingEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "rating_id")
    val ratingId: Long = 0,
    @ColumnInfo(name = "game_id")
    val gameId: Int,
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "count")
    val count: Int,
    @ColumnInfo(name = "percent")
    val percent: Float
){
    fun toRating() = Rating(
        id = id,
        title = title,
        count = count,
        percent = percent
    )
}
