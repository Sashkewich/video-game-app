package project.rawg.mainpage.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "short_screenshots"
)

data class ShortScreenshotEntity(
    @ColumnInfo(name = "game_id")
    val gameId: Int,
    @PrimaryKey
    @ColumnInfo(name = "image")
    val image: String
)
