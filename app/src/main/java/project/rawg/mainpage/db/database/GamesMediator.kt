package project.rawg.mainpage.db.database

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import project.rawg.mainpage.api.GenreType
import project.rawg.mainpage.api.PagingState
import project.rawg.mainpage.di.MainPageModule.Companion.DefaultDispatcher
import project.rawg.mainpage.model.Game
import project.rawg.mainpage.model.GameCategoryModel
import project.rawg.mainpage.repository.base.MainPageLocalRepository
import project.rawg.mainpage.repository.base.MainPageRemoteRepository
import project.rawg.utils.extensions.collectFLow
import javax.inject.Inject

class GamesMediator @Inject constructor(
    private val remoteRepository: MainPageRemoteRepository,
    private val localRepository: MainPageLocalRepository,
    private val genreType: GenreType,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) {

    private val dataStateFlow = MutableStateFlow(convertToModel(PagingState.Initial))

    fun data(): Flow<GameCategoryModel> = dataStateFlow

    suspend fun init(scope: CoroutineScope, refresh: Boolean = false) {
        val state: PagingState<List<Game>> = if (refresh) PagingState.Initial
        else dataStateFlow.value.dataState
        if (state is PagingState.Initial) {
            try {
                val localData =
                    localRepository.getGamesList(scope, genreType.genreTitle).collectFLow(
                        defaultDispatcher,
                        scope
                    ) {
                        it
                    }
                if (localData.isEmpty()) {
                    val data = remoteRepository.initialLoading(genreType.genreTitle)
                    dataStateFlow.emit(convertToModel(PagingState.Content(data)))
                    localRepository.putGamesList(data, genreType.genreTitle)
                } else {
                    dataStateFlow.emit(convertToModel(PagingState.Content(localData)))
                }

            } catch (e: Exception) {
                dataStateFlow.emit(convertToModel(PagingState.Error))
                throw e
            }
        }
    }

    suspend fun tryToLoadMore(index: Int, scope: CoroutineScope) {
        val state = dataStateFlow.value.dataState
        if (state is PagingState.Content && index == state.data.size - 1) {
            loadMore(scope)
        }
    }

    suspend fun refresh(scope: CoroutineScope) {
        if (dataStateFlow.value.dataState is PagingState.Error) {
            init(scope = scope, refresh = true)
        } else {
            remoteRepository.updateParams(
                genre = genreType.genreTitle,
                alreadyLoadedCount = localRepository.getGamesList(scope, genreType.genreTitle)
                    .collectFLow(
                        defaultDispatcher,
                        scope
                    ) {
                        it
                    }.size
            )
            loadMore(scope)
        }
    }

    private suspend fun loadMore(scope: CoroutineScope) {
        val currentData = (dataStateFlow.value.dataState as? PagingState.Content)?.data
            ?: (dataStateFlow.value.dataState as? PagingState.Persist)?.data
            ?: return
        dataStateFlow.emit(convertToModel(PagingState.Paging(currentData)))
        try {
            remoteRepository.updateParams(
                genre = genreType.genreTitle,
                alreadyLoadedCount = localRepository.getGamesList(scope, genreType.genreTitle)
                    .collectFLow(
                        defaultDispatcher,
                        scope
                    ) {
                        it
                    }.size
            )
            val data = remoteRepository.loadMore()
            dataStateFlow.emit(convertToModel(PagingState.Content(currentData.plus(data))))
            localRepository.putGamesList(data, genreType.genreTitle)
        } catch (e: Exception) {
            val flow = localRepository.getGamesList(scope, genreType.genreTitle)
            val localData = flow.collectFLow(defaultDispatcher, scope) {
                it
            }
            if (localData.isEmpty()) {
                dataStateFlow.emit(convertToModel(PagingState.Error))
            } else {
                dataStateFlow.emit(convertToModel(PagingState.Persist(localData)))
            }
            throw e
        }
    }

    private fun convertToModel(state: PagingState<List<Game>>) = GameCategoryModel(
        genreType = genreType,
        dataState = state,
    )
}
