package project.rawg.mainpage.db.dao

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import project.rawg.mainpage.db.model.*

@Dao
interface GamesDao {

    @Query("SELECT * FROM games WHERE genre_type = :genre")
    fun getGames(genre: String): Flow<List<GameEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addGames(games: List<GameEntity>)


    @Query("SELECT * FROM ratings WHERE game_id = :gameId")
    fun getRatings(gameId: Int): Flow<List<RatingEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addRatings(ratings: List<RatingEntity>)


    @Query("SELECT * FROM short_screenshots WHERE game_id = :gameId")
    fun getShortScreenshots(gameId: Int): Flow<List<ShortScreenshotEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addShortScreenshots(shortScreenshots: List<ShortScreenshotEntity>)
}
