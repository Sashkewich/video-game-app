package project.rawg.utils

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow

object FlowCollector {
    @OptIn(ExperimentalCoroutinesApi::class)
    suspend fun <T, G> collectFLow(
        dispatcher: CoroutineDispatcher,
        scope: CoroutineScope,
        flow: Flow<T>,
        func: suspend (T) -> G
    ): G {
        return suspendCancellableCoroutine { continuation ->
            val job = scope.launch {
                flow.collect { flowData ->
                    withContext(dispatcher) {
                        val data = func(flowData)
                        if (continuation.isActive) {
                            continuation.resume(data) {
                                this.cancel()
                            }
                        }
                    }
                    return@collect
                }
            }
            continuation.invokeOnCancellation { job.cancel() }
        }
    }
}
