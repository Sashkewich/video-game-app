package project.rawg.root

import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import dagger.hilt.android.AndroidEntryPoint
import project.rawg.R
import project.rawg.databinding.ActivityRootBinding
import project.rawg.mainpage.ui.MainPageFragment
import project.rawg.utils.extensions.popFeature

@AndroidEntryPoint
class RootActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRootBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_RAWG)
        binding = ActivityRootBinding.inflate(layoutInflater)
        setContentView(binding.root)
        replace(MainPageFragment())
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Are you sure to exit from app?")
        builder.setCancelable(true)
        builder.setNegativeButton("No", DialogInterface.OnClickListener { dialog, _ ->
            dialog.cancel()
        })

        builder.setPositiveButton("Yes", DialogInterface.OnClickListener { _, _ ->
            finish()
        })

        val dialogAlert = builder.create()
        dialogAlert.show()
    }

    private fun replace(
        fragment: Fragment,
        tag: String = fragment::class.java.name
    ) {
        val fragmentManager = this.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction
            .replace(R.id.container, fragment, tag)
            .addToBackStack(tag)
            .commit()
    }
}
